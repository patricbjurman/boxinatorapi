﻿using AutoMapper;
using BoxinatorAPI.Models.Domains;
using BoxinatorAPI.Models.DTOs.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BoxinatorAPI.Profiles
{
    public class CountryProfile : Profile
    {
        public CountryProfile()
        {
            CreateMap<Country, CountryCreate>()
                .ReverseMap();
            CreateMap<Country, CountryEdit>()
                .ReverseMap();
            CreateMap<Country, CountryRead>()
                .ReverseMap();
        }
    }
}
