﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BoxinatorAPI.Migrations
{
    public partial class addedIds : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Countries",
                columns: new[] { "Id", "CountryMultiplier", "Name" },
                values: new object[,]
                {
                    { 1, 0, "Sweden" },
                    { 2, 0, "Norway" },
                    { 3, 0, "Denmark" },
                    { 4, 0, "Argentina" },
                    { 5, 0, "Germany" }
                });

            migrationBuilder.InsertData(
                table: "ShipmentStatuses",
                columns: new[] { "Id", "CreatedAt", "ShipmentId", "Status" },
                values: new object[,]
                {
                    { 5, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 0 },
                    { 4, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 0 },
                    { 3, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 0 },
                    { 2, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 0 },
                    { 1, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 0 }
                });

            migrationBuilder.InsertData(
                table: "Shipments",
                columns: new[] { "Id", "BoxColour", "DestinationCountryId", "ReceiverName", "TotalCost", "UserId", "WeightOption" },
                values: new object[,]
                {
                    { 4, null, null, null, 0, null, 0 },
                    { 3, null, null, null, 0, null, 0 },
                    { 2, null, null, null, 0, null, 0 },
                    { 1, null, null, null, 0, null, 0 }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "AccountType", "BirthDate", "Contactnumber", "CountryId", "Email", "FirstName", "LastName", "Sub", "ZipCode" },
                values: new object[,]
                {
                    { 1, null, null, null, null, "patric.bjurman@se.experis.com", "Patric", "Bjurman", "auth0|6136084e2c6a4500693fefa9", null },
                    { 2, null, null, null, null, "patric.bjurman@se.experis.com", "Patric", "Bjurman", "auth0|6136084e2c6a4500693fefa9", null },
                    { 3, null, null, null, null, "patric.bjurman@se.experis.com", "Patric", "Bjurman", "auth0|6136084e2c6a4500693fefa9", null }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "ShipmentStatuses",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "ShipmentStatuses",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "ShipmentStatuses",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "ShipmentStatuses",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "ShipmentStatuses",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Shipments",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Shipments",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Shipments",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Shipments",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3);
        }
    }
}
