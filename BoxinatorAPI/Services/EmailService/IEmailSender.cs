﻿using BoxinatorAPI.Models.Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BoxinatorAPI.Services.EmailService
{
    public interface IEmailSender
    {
        public Task SendReceiptEmail(string to, Shipment order);
    }
}
