﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;


namespace BoxinatorAPI.Models.Domains
{

    public class ShipmentStatus
    {
        // Pk
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        // Fields

        public int? ShipmentId { get; set; }

        public ShipmentStatusType Status { get; set; }

        public DateTime CreatedAt { get; set; }

        // Relationships
        public Shipment Shipment { get; set; }
    }
}
