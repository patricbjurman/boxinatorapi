﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BoxinatorAPI.Models.Domains
{
    public class Shipment
    {
        // Pk
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        // Fields
        [MaxLength(100)]
        public string ReceiverName { get; set; }


        [MaxLength(20)]
        public string BoxColour { get; set; }

        public int WeightOption { get; set; }

        public int TotalCost { get; set; }

        public int? DestinationCountryId { get; set; }

        public int? UserId { get; set; }

        // Relationshipss

        public User User { get; set; }
        public Country DestinationCountry { get; set; }
        public ICollection<ShipmentStatus> ShipmentStatus { get; set; }
    }
}
