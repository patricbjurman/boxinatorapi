﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using BoxinatorAPI.Models.Domains;


namespace BoxinatorAPI.Models.DTOs.User
{
    public class UserEdit
    {
        [MaxLength(100)]
        public string FirstName { get; set; }

        [MaxLength(100)]
        public string LastName { get; set; }

        public DateTime? BirthDate { get; set; }

        public int? ZipCode { get; set; }

        [MaxLength(14)]
        public string Contactnumber { get; set; }

        public int CountryId { get; set; }

    }
}
