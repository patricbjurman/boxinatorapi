﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using BoxinatorAPI.Models.Domains;
using BoxinatorAPI.Models.DTOs.Settings;

namespace BoxinatorAPI.Models.DTOs.User
{
    public class UserRead
    {
        // Key
        [Key]
        public int Id { get; set; }

        [MaxLength(100)]
        public string FirstName { get; set; }

        [MaxLength(100)]
        public string LastName { get; set; }

        [MaxLength(100)]
        public string Email { get; set; }

        public DateTime? BirthDate { get; set; }

        public int? ZipCode { get; set; }
        
        [MaxLength(14)]
        public string Contactnumber { get; set; }

        public CountryRead Country { get; set; }
         
        //public AccountType? AccountType { get; set; }
    }
}
