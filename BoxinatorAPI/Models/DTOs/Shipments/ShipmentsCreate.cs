﻿using BoxinatorAPI.Models.Domains;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BoxinatorAPI.Models.DTOs.Shipments
{
    public class ShipmentsCreate 
    {
        [Required]
        [MaxLength(100)]
        public string ReceiverName { get; set; }

        [Required]
        [MaxLength(20)]
        public string BoxColour { get; set; }

        [Required]
        public int WeightOption { get; set; }

        //[Required]
        //public int TotalCost { get; set; }

        [Required]
        public int DestinationCountryId { get; set; }

        public String Email { get; set; }
    }
}
