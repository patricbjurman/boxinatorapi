﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BoxinatorAPI.Models;
using BoxinatorAPI.Models.DTOs.Shipments;
using BoxinatorAPI.Models.Domains;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using BoxinatorAPI.Models.DTOs.ShipmentStatus;
using BoxinatorAPI.Services.EmailService;
using System.Net.Mime;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace BoxinatorAPI.Controllers
{
    [Route("api/v1/shipments")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class ShipmentsController : ControllerBase
    {
        private readonly BoxinatorDbContext _context;
        private readonly IMapper _mapper;
        private readonly IEmailSender _emailSender;

        public ShipmentsController(BoxinatorDbContext context, IMapper mapper, IEmailSender emailSender)
        {
            _emailSender = emailSender;
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Checks if the current user is Admin or not.
        /// </summary>
        /// <returns></returns>
        private bool IsAdmin()
        {
            IEnumerable<string> permissions = (from c in User.Claims
                                               where c.Type == "permissions"
                                               select c.Value);
            if (permissions.Contains("Admin"))
                return true;
            else
                return false;
        }

        /// <summary>
        /// Returns the Users ID. -1 if guest user.
        /// </summary>
        /// <returns></returns>
        private async Task<int> GetUserId()
        {
            string sub = User.Identity.Name;
            if (sub == null)
            {
                return -1;
            }
            User user = await _context.Users.SingleOrDefaultAsync(u => u.Sub == sub);
            if (user != null)
            {
                return user.Id;
            }
            return -1;
        }

        /// <summary>
        /// Returns all shipments that is not Completed or Cancelled.
        /// </summary>
        /// <response code = "200">Request ok.</response>
        /// <returns>Returns a list of shipments</returns>
        [HttpGet]
        [Authorize]
        public async Task<ActionResult<IEnumerable<ShipmentsRead>>> GetAllShipments()
        {
            if (IsAdmin())
            {
                var shipments = await _context.Shipments
                    .Where((s) =>
                        _context.ShipmentStatuses.Where(status => status.ShipmentId == s.Id).OrderByDescending(s => s.CreatedAt).FirstOrDefault().Status != ShipmentStatusType.COMPLETED &&
                        _context.ShipmentStatuses.Where(status => status.ShipmentId == s.Id).OrderByDescending(s => s.CreatedAt).FirstOrDefault().Status != ShipmentStatusType.CANCELLED)
                    .OrderByDescending(s => _context.ShipmentStatuses.Where(status => status.ShipmentId == s.Id).OrderByDescending(s => s.CreatedAt).FirstOrDefault().CreatedAt)
                    .ToArrayAsync();

                var allShipmentsDTO = _mapper.Map<List<ShipmentsRead>>(shipments);
                return Ok(allShipmentsDTO);
            }
            else
            {
                string userSub = User.Identity.Name;

                var shipments = await _context.Shipments
                    .Where(s => s.User.Sub == userSub && 
                        _context.ShipmentStatuses.Where(status => status.ShipmentId == s.Id).OrderByDescending(s => s.CreatedAt).FirstOrDefault().Status != ShipmentStatusType.COMPLETED &&
                        _context.ShipmentStatuses.Where(status => status.ShipmentId == s.Id).OrderByDescending(s => s.CreatedAt).FirstOrDefault().Status != ShipmentStatusType.CANCELLED)
                    .OrderByDescending(s => _context.ShipmentStatuses.Where(status => status.ShipmentId == s.Id).OrderByDescending(s => s.CreatedAt).FirstOrDefault().CreatedAt)
                    .ToArrayAsync();
                var shipmentsDTO = _mapper.Map<List<ShipmentsRead>>(shipments);

                return Ok(shipmentsDTO);
            }
        }

        /// <summary>
        /// Returns all completed shipments.
        /// </summary>
        /// <response code = "200">Request ok.</response>
        /// <returns>Returns a list of shipments.</returns>
        [HttpGet("complete")]
        [Authorize]
        public async Task<ActionResult<IEnumerable<ShipmentsRead>>> GetComplete()
        {

            if (IsAdmin())
            {
                var allShipments = await _context.Shipments
                    .Where(s => _context.ShipmentStatuses.Where(status => status.ShipmentId == s.Id).OrderByDescending(s => s.CreatedAt).FirstOrDefault().Status == ShipmentStatusType.COMPLETED)
                    .OrderByDescending(s => _context.ShipmentStatuses.Where(status => status.ShipmentId == s.Id).OrderByDescending(s => s.CreatedAt).FirstOrDefault().CreatedAt)
                    .ToArrayAsync();
                var allShipmentsDTO = _mapper.Map<List<ShipmentsRead>>(allShipments);
                return Ok(allShipmentsDTO);
            }
            else
            {
                string userName = User.Identity.Name;
                var allShipments = await _context.Shipments
                    .Where(s => s.User.Sub == userName && _context.ShipmentStatuses.Where(status => status.ShipmentId == s.Id).OrderByDescending(s => s.CreatedAt).FirstOrDefault().Status == ShipmentStatusType.COMPLETED)
                    .OrderByDescending(s => _context.ShipmentStatuses.Where(status => status.ShipmentId == s.Id).OrderByDescending(s => s.CreatedAt).FirstOrDefault().CreatedAt)
                    .ToArrayAsync();
                var allShipmentsDTO = _mapper.Map<List<ShipmentsRead>>(allShipments);
                return Ok(allShipmentsDTO);
            }
        }

        /// <summary>
        /// Returns all cancelled shipments.
        /// </summary>
        /// <response code = "200">Request ok.</response>
        /// <returns>Returns a list of shipments.</returns>
        [HttpGet("cancelled")]
        [Authorize]
        public async Task<ActionResult<IEnumerable<ShipmentsRead>>> GetCancelled()
        {
            if (IsAdmin())
            {
                var allShipments = await _context.Shipments
                    .Where(s => _context.ShipmentStatuses.Where(status => status.ShipmentId == s.Id).OrderByDescending(s => s.CreatedAt).FirstOrDefault().Status == ShipmentStatusType.CANCELLED)
                    .OrderByDescending(s => _context.ShipmentStatuses.Where(status => status.ShipmentId == s.Id).OrderByDescending(s => s.CreatedAt).FirstOrDefault().CreatedAt)
                    .ToArrayAsync();
                var allShipmentsDTO = _mapper.Map<List<ShipmentsRead>>(allShipments);
                return Ok(allShipmentsDTO);
            }
            else
            {
                string userName = User.Identity.Name;
                var allShipments = await _context.Shipments
                    .Where(s => s.User.Sub == userName && _context.ShipmentStatuses.Where(status => status.ShipmentId == s.Id).OrderByDescending(s => s.CreatedAt).FirstOrDefault().Status == ShipmentStatusType.CANCELLED)
                    .OrderByDescending(s => _context.ShipmentStatuses.Where(status => status.ShipmentId == s.Id).OrderByDescending(s => s.CreatedAt).FirstOrDefault().CreatedAt)
                    .ToArrayAsync();
                var allShipmentsDTO = _mapper.Map<List<ShipmentsRead>>(allShipments);
                return Ok(allShipmentsDTO);
            }
        }

        /// <summary>
        /// Posts a new shipment to the database.
        /// </summary>
        /// <param name="shipment">The shipment you want to post.</param>
        /// <response code = "200">Request ok.</response>
        /// <response code = "400">Bad request. Probably something wrong with the body.</response>
        /// <returns>The posted shipment</returns>
        [HttpPost]
        public async Task<ActionResult<ShipmentsRead>> Post([FromBody] ShipmentsCreate shipment)
        {
            if (!new int[] { 1, 2, 5, 8 }.Contains(shipment.WeightOption))
            {
                return BadRequest();
            }


            var id = await GetUserId();
            bool isGuestUsage = false;

            if (id == -1)
            {
                isGuestUsage = true;
                var email = shipment.Email;
                if (email == null)
                {
                    return BadRequest();
                }

                User user = await _context.Users.SingleOrDefaultAsync(u => u.Email == email);

                if (user == null)
                {
                    user = new User { Email = email };
                    await _context.AddAsync(user);
                    await _context.SaveChangesAsync();
                }
                id = user.Id;
            }

            var domainShipment = _mapper.Map<Shipment>(shipment);
            domainShipment.UserId = id;
            var destinationCountry = await _context.Countries.FindAsync(domainShipment.DestinationCountryId);

            if (destinationCountry == null)
            {
                return BadRequest();
            }

            domainShipment.TotalCost = 200 + destinationCountry.CountryMultiplier * domainShipment.WeightOption;

            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {

                    await _context.AddAsync(domainShipment);
                    await _context.SaveChangesAsync();
                    var shipmentStatus = new ShipmentStatus { CreatedAt = DateTime.Now, ShipmentId = domainShipment.Id, Status = ShipmentStatusType.CREATED };
                    
                    await _context.AddAsync(shipmentStatus);
                    await _context.SaveChangesAsync();

                    if (isGuestUsage)
                    {
                        //await _emailSender.SendReceiptEmail(shipment.Email, domainShipment);
                    }
                   
                    transaction.Commit();
                }
                catch (DbUpdateException)
                {
                    transaction.Rollback();
                    return BadRequest();
                }
                catch
                {
                    transaction.Rollback();
                    throw;
                }
            }
            return Ok(_mapper.Map<ShipmentsRead>(domainShipment));
        }

        /// <summary>
        /// Gets a shipment based by ID.
        /// </summary>
        /// <param name="id">Id of the shipment you want to fetch.</param>
        /// <response code = "404">Not found. Shipment not found.</response>
        /// <returns>The shipment requested.</returns>
        [HttpGet("{id}")]
        [Authorize]
        public async Task<ActionResult<ShipmentsDetailedRead>> GetById(int id)
        {
            var userAuth = User.Identity.Name;
            Shipment shipment;
            if (IsAdmin())
            {
                shipment = await _context.Shipments.Include(s => s.DestinationCountry).FirstOrDefaultAsync((s) => s.Id == id);
                shipment = await _context.Shipments.Include(s => s.User).FirstOrDefaultAsync((s) => s.Id == id);
            } else
            {
                var user = await _context.Users.FirstOrDefaultAsync((u) => u.Sub == userAuth);
                shipment = await _context.Shipments.Include(s => s.DestinationCountry).FirstOrDefaultAsync((s) => s.UserId == user.Id && s.Id == id);
            }
                

            if (shipment == null)
            {
                return NotFound();
            }
            var shipmentsDTO = _mapper.Map<ShipmentsDetailedRead>(shipment);
            var shipmentStatus = await _context.ShipmentStatuses.Where(s => s.ShipmentId == id).OrderByDescending(s => s.CreatedAt).ToArrayAsync();
            shipmentsDTO.ShipmentStatus = _mapper.Map<List<ShipmentStatusRead>>(shipmentStatus);
            
            return Ok(shipmentsDTO);
        }

        /// <summary>
        /// Gets a specific users shipments.
        /// </summary>
        /// <param name="id">The id of the users shipments you want to fetch.</param>
        /// <response code = "404">Not found. Customer not found.</response>
        /// <returns>The specific users shipments in a list.</returns>
        [HttpGet("customer/{id}")]
        [Authorize("Admin")]
        public async Task<ActionResult<IEnumerable<ShipmentsRead>>> GetByCustomerId(int id)
        {
            var user = await _context.Users.FirstOrDefaultAsync((u) => u.Id == id);
            if (user == null)
            {
                return NotFound();
            }

            var shipments = await _context.Shipments.Where((s) => s.UserId == user.Id).ToArrayAsync();
            return Ok(_mapper.Map<List<ShipmentsRead>>(shipments));
        }

        /// <summary>
        /// Updates a shipment.
        /// </summary>
        /// <param name="id">The id of the shipment you want to update.</param>
        /// <param name="value">The Shipment status type you want to update to.</param>
        /// <response code = "200">Request ok. Shipment updated.</response>
        /// <response code = "404">Not found. Customer not found.</response>
        /// <returns></returns>
        [HttpPost("shipmentstatus/{id}")]
        [Authorize]
        public async Task<ActionResult<ShipmentStatusRead>> UpdateShipmentStatus(int id, [FromBody]ShipmentStatusCreate value)
        {
            if (IsAdmin())
            {
                var shipment = await _context.Shipments.FirstOrDefaultAsync((s) => s.Id == id);
                if (shipment == null)
                {
                    return NotFound();
                }
                ShipmentStatus status = new() { ShipmentId = id, Status = value.Status, CreatedAt = DateTime.Now };
                await _context.ShipmentStatuses.AddAsync(status);
                await _context.SaveChangesAsync();
                return Ok(_mapper.Map<ShipmentStatusRead>(status));
            }
            else
            {
                if (value.Status != ShipmentStatusType.CANCELLED)
                {
                    return BadRequest();
                }

                var userAuth = User.Identity.Name;
                var user = await _context.Users.FirstOrDefaultAsync((u) => u.Sub == userAuth);
                var shipment = await _context.Shipments.FirstOrDefaultAsync((s) => s.UserId == user.Id && s.Id == id);
                if (shipment == null)
                {
                    return NotFound();
                }
                ShipmentStatus status = new() { ShipmentId = id, Status = ShipmentStatusType.CANCELLED, CreatedAt = DateTime.Now };
                await _context.ShipmentStatuses.AddAsync(status);
                await _context.SaveChangesAsync();
                return Ok(_mapper.Map<ShipmentStatusRead>(status));
            }
        }

        /// <summary>
        /// Deletes a shipment.
        /// </summary>
        /// <param name="id">The id of the shipment you want to delete.</param>
        /// <response code = "404">Not found. Shipment not found.</response>
        /// <response code = "400">Bad request. Probably something wrong in the body.</response>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [Authorize("Admin")]
        public async Task<ActionResult> Delete(int id)
        {

            var shipment = await _context.Shipments.FindAsync(id);

            if (shipment == null)
            {
                return NotFound();
            }

            try
            {
                _context.Shipments.Remove(shipment);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {

                throw;
            }

            return NoContent();
        }
    }
}
