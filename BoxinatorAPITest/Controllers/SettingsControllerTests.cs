﻿using AutoMapper;
using BoxinatorAPI.Controllers;
using BoxinatorAPI.Models;
using BoxinatorAPI.Models.Domains;
using BoxinatorAPI.Models.DTOs.Settings;
using BoxinatorAPI.Profiles;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualBasic.CompilerServices;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;


namespace BoxinatorAPITest.Controllers
{
    public class SettingsControllerTests
    {
        private DbContextOptions<BoxinatorDbContext> dbContextOptions = new DbContextOptionsBuilder<BoxinatorDbContext>()
                .UseInMemoryDatabase(databaseName: "TestSettingsDb")
                .Options;
        private BoxinatorDbContext context;
        private SettingsController controller;
        private int currentId = 5;

        [OneTimeSetUp]
        public void Setup()
        {
            context = new BoxinatorDbContext(dbContextOptions);
            SeedDb();

            Mapper mapper = new Mapper(new MapperConfiguration(conf => conf.AddProfiles(new Profile[] { new UserProfile(), new ShipmentProfile(), new CountryProfile() })));
            controller = new SettingsController(context, mapper);
        }

        private void SeedDb()
        {
            var countries = new List<Country>
            {
                new Country { Id = 1, Name = "Sweden", CountryMultiplier = 0 },
                new Country { Id = 2, Name = "Norway", CountryMultiplier = 0 },
                new Country { Id = 3, Name = "Denmark", CountryMultiplier = 0 },
                new Country { Id = 4, Name = "Argentina", CountryMultiplier = 5 },
                new Country { Id = 5, Name = "Germany", CountryMultiplier = 3 }
            };
            context.Countries.AddRange(countries);

            context.SaveChanges();
        }



        [Test]
        public async Task Get_GetCountrys_GetCountryList()
        {    
            var res = await controller.Get();
            Xunit.Assert.IsType<OkObjectResult>(res.Result);
            var ok = (OkObjectResult)res.Result;
            var countries = (List<CountryRead>)ok.Value;
            countries.Count.Should().Be(5);
            countries[0].Name.Should().Be("Argentina");
            countries[4].Name.Should().Be("Sweden");
        }

        // TODO fix in db.
        //[Test]
        public async Task Post_AddCountryWithNullValue_BadRequest()
        {
            var newCountry = new CountryCreate { Name = null, CountryMultiplier = 1 };
            currentId++;
            var res = await controller.Post(newCountry);
            Xunit.Assert.IsType<BadRequestObjectResult>(res.Result);
            
        }

        //[Test]
        public async Task Post_AddCountrywithInvalidData_BadRequest()
        {
            var newCountry = new CountryCreate { Name = "Sweden", CountryMultiplier = 1 };
            currentId++;
            var res = await controller.Post(newCountry);
            Xunit.Assert.IsType<BadRequestObjectResult>(res.Result); 
        }

        [Test]
        public async Task Post_AddCountry_Sucsses()
        {
            var newCountry = new CountryCreate { Name = "test", CountryMultiplier = 1 };
            currentId++;
            int id = currentId;
            var res = await controller.Post(newCountry);
            Xunit.Assert.IsType<CreatedAtActionResult>(res.Result);
            var create = (CreatedAtActionResult)res.Result;
            var country = (CountryRead)create.Value;
            country.Id.Should().Be(id);
            country.Name.Should().Be("test");
            country.CountryMultiplier.Should().Be(1);
        }


        [Test]
        public async Task Put_UpdateCountry_Sucsses()
        {
            int id = 1;
            var countryEdit = new CountryEdit { CountryMultiplier = 3, Name = "test" };
            var res = await controller.Put(id, countryEdit);
            Xunit.Assert.IsType<NoContentResult>(res);
            var country = await context.Countries.FindAsync(id);
            country.Name.Should().Be("test");
            country.CountryMultiplier.Should().Be(3);
        }

        [Test]
        public async Task Put_UpdateCountry_NotFound()
        {
            var countryEdit = new CountryEdit { CountryMultiplier = 3, Name = "test" };
            var res = await controller.Put(0, countryEdit);
            Xunit.Assert.IsType<NotFoundResult>(res);
        }


    }
}
