﻿using AutoMapper;
using BoxinatorAPI.Controllers;
using BoxinatorAPI.Models;
using BoxinatorAPI.Models.Domains;
using BoxinatorAPI.Models.DTOs.Shipments;
using BoxinatorAPI.Models.DTOs.ShipmentStatus;
using BoxinatorAPI.Profiles;
using BoxinatorAPI.Services.EmailService;
using FluentAssertions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace BoxinatorAPITest.Controllers
{
    class ShipmentsControllerTests
    {
        private DbContextOptions<BoxinatorDbContext> dbContextOptions = new DbContextOptionsBuilder<BoxinatorDbContext>()
                .UseInMemoryDatabase(databaseName: "TestShipmentsDb")
                .ConfigureWarnings(x => x.Ignore(InMemoryEventId.TransactionIgnoredWarning))
                .Options;
        private BoxinatorDbContext context;
        private Mapper mapper;
        private int currentShipmentId;
        private int currentUserId;

        [OneTimeSetUp]
        public void Setup()
        {
            context = new BoxinatorDbContext(dbContextOptions);
            SeedDb();

            mapper = new Mapper(new MapperConfiguration(conf => conf.AddProfiles(new Profile[] { new UserProfile(), new ShipmentProfile(), new CountryProfile() })));
        }

        private void SeedDb()
        {
            var countries = new List<Country>
            {
                new Country { Id = 1, Name = "Sweden", CountryMultiplier = 0 },
                new Country { Id = 2, Name = "Norway", CountryMultiplier = 0 },
                new Country { Id = 3, Name = "Denmark", CountryMultiplier = 0 },
                new Country { Id = 4, Name = "Argentina", CountryMultiplier = 5 },
                new Country { Id = 5, Name = "Germany", CountryMultiplier = 3 }
            };
            context.Countries.AddRange(countries);

            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    FirstName = "Patric",
                    LastName = "Bju",
                    Email = "patric.bjurman@se.experis.com",
                    Sub = "auth0|6136084e2c6a4500693fefa9",
                    BirthDate = new DateTime(1990, 1, 23),
                    Contactnumber = "000001",
                    CountryId = 2,
                    ZipCode = 11122
                },
                new User
                {
                    Id = 2,
                    FirstName = "Emil",
                    LastName = "Cle",
                    Email = "emilcle@gmail.com",
                    Sub = "auth0|612f2c14eeeb2e0068b262b0",
                    BirthDate = new DateTime(1990, 1, 23),
                    Contactnumber = "000000",
                    CountryId = 1,
                    ZipCode = 11122
                },
            };

            context.Users.AddRange(users);

            var shipments = new List<Shipment> {
                new Shipment { Id = 1, DestinationCountryId = 1, UserId = 1, BoxColour= "#000",  TotalCost=100, ReceiverName="test1", WeightOption=5 },
                new Shipment { Id = 2, DestinationCountryId = 2, UserId = 2, BoxColour= "#009",  TotalCost=999, ReceiverName="test2", WeightOption=5 },
                new Shipment { Id = 3, DestinationCountryId = 3, UserId = 1, BoxColour= "#090",  TotalCost=111, ReceiverName="test3", WeightOption=2 },
                new Shipment { Id = 4, DestinationCountryId = 4, UserId = 2, BoxColour= "#900",  TotalCost=222, ReceiverName="tesT4", WeightOption=8 },
                new Shipment { Id = 5, DestinationCountryId = 5, UserId = 1, BoxColour= "#ff0",  TotalCost=1337, ReceiverName="test5", WeightOption=1 },
                new Shipment { Id = 6, DestinationCountryId = 6, UserId = 2, BoxColour= "#fff",  TotalCost=1336, ReceiverName="test6", WeightOption=1 },
            };

            context.Shipments.AddRange(shipments);


            var shipmentStatuses = new List<ShipmentStatus>
            {
                new ShipmentStatus { Id = 1, ShipmentId = 1, Status = ShipmentStatusType.CREATED, CreatedAt = new DateTime(2021, 01, 05) },
                new ShipmentStatus { Id = 2, ShipmentId = 1, Status = ShipmentStatusType.CANCELLED, CreatedAt = new DateTime(2021, 01, 06) },

                new ShipmentStatus { Id = 3, ShipmentId = 2, Status = ShipmentStatusType.CREATED, CreatedAt = new DateTime(2021, 03, 05) },
                new ShipmentStatus { Id = 4, ShipmentId = 2, Status = ShipmentStatusType.INTRANSIT, CreatedAt = new DateTime(2021, 03, 06) },
                new ShipmentStatus { Id = 5, ShipmentId = 2, Status = ShipmentStatusType.CANCELLED, CreatedAt = new DateTime(2021, 03, 07) },

                new ShipmentStatus { Id = 6, ShipmentId = 3, Status = ShipmentStatusType.CREATED, CreatedAt = new DateTime(2021, 07, 05) },
                new ShipmentStatus { Id = 7, ShipmentId = 3, Status = ShipmentStatusType.RECIEVED, CreatedAt = new DateTime(2021, 07, 06) },

                new ShipmentStatus { Id = 8, ShipmentId = 4, Status = ShipmentStatusType.CREATED, CreatedAt = new DateTime(2021, 02, 05) },
                new ShipmentStatus { Id = 9, ShipmentId = 4, Status = ShipmentStatusType.INTRANSIT, CreatedAt = new DateTime(2021, 02, 06) },

                new ShipmentStatus { Id = 10, ShipmentId = 5, Status = ShipmentStatusType.CREATED, CreatedAt = new DateTime(2021, 05, 05) },
                new ShipmentStatus { Id = 11, ShipmentId = 5, Status = ShipmentStatusType.COMPLETED, CreatedAt = new DateTime(2021, 05, 06) },

                new ShipmentStatus { Id = 12, ShipmentId = 6, Status = ShipmentStatusType.CREATED, CreatedAt = new DateTime(2021, 08, 04) },
                new ShipmentStatus { Id = 13, ShipmentId = 6, Status = ShipmentStatusType.INTRANSIT, CreatedAt = new DateTime(2021, 08, 05) },
                new ShipmentStatus { Id = 14, ShipmentId = 6, Status = ShipmentStatusType.COMPLETED, CreatedAt = new DateTime(2021, 08, 06) },
            };

            currentShipmentId = shipments.Count;
            currentUserId = users.Count;

            context.ShipmentStatuses.AddRange(shipmentStatuses);

            context.SaveChanges();
        }


        private ShipmentsController GetUserShipmentsController()
        {
            var controller = new ShipmentsController(context, mapper, null);
            var user = new ClaimsPrincipal(new ClaimsIdentity(new Claim[]
            {
                new Claim(ClaimTypes.Name, "auth0|612f2c14eeeb2e0068b262b0")
            }, "mock"));
            controller.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext() { User = user }
            };
            return controller;
        }

        private ShipmentsController GetNoUserShipmentsController()
        {
            var controller = new ShipmentsController(context, mapper, null);
            var user = new ClaimsPrincipal(new ClaimsIdentity(new Claim[]
            {
            }, "mock"));
            controller.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext() { User = user }
            };
            return controller;
        }


        private ShipmentsController GetAdminShipmentsController()
        {
            var controller = new ShipmentsController(context, mapper, null);
            var user = new ClaimsPrincipal(new ClaimsIdentity(new Claim[]
            {
                new Claim(ClaimTypes.Name, "auth0|612f2c14eeeb2e0068b262b0"),
                new Claim("permissions", "Admin")
            }, "mock"));
            controller.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext() { User = user }
            };
            return controller;
        }


        [Test, Order(1)]
        public async Task GetAllShipments_UserGet_RetuenAllTheUsersOreder()
        {
            // Controller set up
            var controller = GetUserShipmentsController();

            // 
            int numShipments = 1;
            int ShipmentId = 4;

            var res = await controller.GetAllShipments();

            // Asserts
            var ok = (OkObjectResult)res.Result;
            var shipments = (List<ShipmentsRead>)ok.Value;
            shipments.Count.Should().Be(numShipments);
            shipments[0].Id.Should().Be(ShipmentId);
        }

        [Test, Order(1)]
        public async Task GetAllShipments_AdminGet_RetuenAllOreder()
        {
            // Controller set up
            var controller = GetAdminShipmentsController();

            // 
            int numShipments = 2;
            int firstShipmentId = 3;
            int lastShipmentId = 4;

            var res = await controller.GetAllShipments();

            // Asserts
            var ok = (OkObjectResult)res.Result;
            var shipments = (List<ShipmentsRead>)ok.Value;
            shipments.Count.Should().Be(numShipments);
            shipments[0].Id.Should().Be(firstShipmentId);
            shipments[1].Id.Should().Be(lastShipmentId);
        }


        [Test, Order(1)]
        public async Task GetComplete_UserGet__RetuenAllTheUsersCompletedOreder()
        {
            // Controller set up
            var controller = GetUserShipmentsController();

            // 
            int numShipments = 1;
            int ShipmentId = 6;

            var res = await controller.GetComplete();

            // Asserts
            var ok = (OkObjectResult)res.Result;
            var shipments = (List<ShipmentsRead>)ok.Value;
            shipments.Count.Should().Be(numShipments);
            shipments[0].Id.Should().Be(ShipmentId);
        }



        [Test, Order(1)]
        public async Task GetComplete_AdminGet_RetuenAllCompletedOreder()
        {
            // Controller set up
            var controller = GetAdminShipmentsController();

            // 
            int numShipments = 2;
            int firstShipmentId = 6;
            int lastShipmentId = 5;

            var res = await controller.GetComplete();

            // Asserts
            var ok = (OkObjectResult)res.Result;
            var shipments = (List<ShipmentsRead>)ok.Value;
            shipments.Count.Should().Be(numShipments);
            shipments[0].Id.Should().Be(firstShipmentId);
            shipments[1].Id.Should().Be(lastShipmentId);
        }


        [Test, Order(1)]
        public async Task GetCancelled_UserGet_RetuenAllTheUsereCancelledOreder()
        {
            // Controller set up
            var controller = GetUserShipmentsController();

            // 
            int numShipments = 1;
            int ShipmentId = 2;

            var res = await controller.GetCancelled();

            // Asserts
            var ok = (OkObjectResult)res.Result;
            var shipments = (List<ShipmentsRead>)ok.Value;
            shipments.Count.Should().Be(numShipments);
            shipments[0].Id.Should().Be(ShipmentId);
        }



        [Test, Order(1)]
        public async Task GetCancelled_AdminGet_RetuenAllCancelledOreder()
        {
            // Controller set up
            var controller = GetAdminShipmentsController();

            // 
            int numShipments = 2;
            int firstShipmentId = 2;
            int lastShipmentId = 1;

            var res = await controller.GetCancelled();

            // Asserts
            var ok = (OkObjectResult)res.Result;
            var shipments = (List<ShipmentsRead>)ok.Value;
            shipments.Count.Should().Be(numShipments);
            shipments[0].Id.Should().Be(firstShipmentId);
            shipments[1].Id.Should().Be(lastShipmentId);
        }


        [Test, Order(2)]
        public async Task Post_CreateShipmentWithInvalidWeightOption_BadRequestResult()
        {
            // Controller set up
            var controller = GetUserShipmentsController();

            // 
            var shipments = new ShipmentsCreate
            {
                BoxColour = "#000",
                WeightOption = 100,
                DestinationCountryId = 1,
                ReceiverName = "test"
            };

            var res = await controller.Post(shipments);

            // Asserts
            res.Result.Should().BeOfType(typeof(BadRequestResult));
        }

        [Test, Order(2)]
        public async Task Post_CreateShipmentWithNotExistingDestinationCountryId_BadRequestResult()
        {
            // Controller set up
            var controller = GetUserShipmentsController();

            // 
            var shipments = new ShipmentsCreate
            {
                BoxColour = "#000",
                WeightOption = 1,
                DestinationCountryId = 100,
                ReceiverName = "test"
            };

            var res = await controller.Post(shipments);

            // Asserts
            res.Result.Should().BeOfType(typeof(BadRequestResult));
        }

        [Test, Order(2)]
        public async Task Post_CreateShipmentWithNóTokenOrEmail_BadRequestResult()
        {
            // Controller set up
            var controller = GetNoUserShipmentsController();

            // 
            var shipments = new ShipmentsCreate
            {
                BoxColour = "#000",
                WeightOption = 1,
                DestinationCountryId = 1,
                ReceiverName = "test"
            };

            var res = await controller.Post(shipments);

            // Asserts
            res.Result.Should().BeOfType(typeof(BadRequestResult));
        }

        [Test, Order(2)]
        public async Task Post_CreateShipmentWithToken_ReturnsTheNewShipment()
        {
            // Controller set up
            var controller = GetUserShipmentsController();

            // 
            var newShipments = new ShipmentsCreate
            {
                BoxColour = "#000",
                WeightOption = 1,
                DestinationCountryId = 1,
                ReceiverName = "test"
            };
            currentShipmentId++;
            var id = currentShipmentId;

            var res = await controller.Post(newShipments);
            var created = (OkObjectResult)res.Result;
            var shipment = (ShipmentsRead)created.Value;

            // Asserts
            res.Result.Should().BeOfType(typeof(OkObjectResult));
            shipment.Id.Should().Be(id);
            shipment.TotalCost.Should().Be(200 + context.Countries.Find(newShipments.DestinationCountryId).CountryMultiplier * newShipments.WeightOption);
            shipment.UserId = 2;
        }


        [Test, Order(2)]
        public async Task Post_CreateShipmentWithEmail_ReturnsTheNewShipment()
        {
            // Controller set up

            var emailTest = new EmailTest();
            var controller = new ShipmentsController(context, mapper, emailTest);
            var user = new ClaimsPrincipal(new ClaimsIdentity(new Claim[]
            {
            }, "mock"));
            controller.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext() { User = user }
            };

            // 
            var newShipments = new ShipmentsCreate
            {
                BoxColour = "#000",
                WeightOption = 1,
                DestinationCountryId = 4,
                ReceiverName = "test",
                Email = "test.test@test.com"
            };
            currentShipmentId++;
            var id = currentShipmentId;
            currentUserId++;
            var userId = currentUserId;

            var res = await controller.Post(newShipments);
            var created = (OkObjectResult)res.Result;
            var shipment = (ShipmentsRead)created.Value;

            // Asserts
            res.Result.Should().BeOfType(typeof(OkObjectResult));
            shipment.Id.Should().Be(id);
            shipment.TotalCost.Should().Be(200 + context.Countries.Find(newShipments.DestinationCountryId).CountryMultiplier * newShipments.WeightOption);
            shipment.UserId.Should().Be(userId);

            var newUser = context.Users.Find(userId);

            newUser.LastName.Should().BeNull();
            newUser.FirstName.Should().BeNull();
            newUser.Sub.Should().BeNull();
            newUser.ZipCode.Should().BeNull();
            newUser.CountryId.Should().BeNull();
            newUser.BirthDate.Should().BeNull();
            newUser.Email.Should().Be(newShipments.Email);

            emailTest.To.Should().Be(newShipments.Email);
            emailTest.Order.ReceiverName.Should().Be(newShipments.ReceiverName);



        }

        class EmailTest : IEmailSender
        {
            public string To { get; set; }
            public Shipment Order { get; set; }

            public async Task SendReceiptEmail(string to, Shipment order)
            {

                To = to;
                Order = order;
                await Task.Run(() => { });
            }
        }

        [Test, Order(1)]
        public async Task GetById_AdminGet_ReturnTheOrder()
        {
            var controller = GetAdminShipmentsController();

            var id = 1;
            var expectedShipment = context.Shipments.Find(id);
            var numStatus = 2;


            var res = await controller.GetById(id);
            var ok = (OkObjectResult)res.Result;
            var shipment = (ShipmentsDetailedRead)ok.Value;

            
            res.Result.Should().BeOfType(typeof(OkObjectResult));
            shipment.ReceiverName.Should().Be(expectedShipment.ReceiverName);
            shipment.WeightOption.Should().Be(expectedShipment.WeightOption);
            shipment.DestinationCountry.Id.Should().Be(expectedShipment.DestinationCountryId);
            shipment.BoxColour.Should().Be(expectedShipment.BoxColour);
            shipment.ShipmentStatus.Count.Should().Be(numStatus);
        }

        [Test, Order(1)]
        public async Task GetById_AdminGetNotExistingShioment_NotFoundResult()
        {
            var controller = GetUserShipmentsController();

            var id = 100;

            var res = await controller.GetById(id);
            res.Result.Should().BeOfType(typeof(NotFoundResult));
        }

        [Test, Order(1)]
        public async Task GetById_UserGetNoItsShipment_NotFoundResult()
        {
            var controller = GetUserShipmentsController();

            var id = 1;

            var res = await controller.GetById(id);
            res.Result.Should().BeOfType(typeof(NotFoundResult));
        }


        [Test, Order(1)]
        public async Task GetById_UserGet_ReturnTheOrder()
        {
            var controller = GetUserShipmentsController();

            var id = 2;
            var expectedShipment = context.Shipments.Find(id);
            var numStatus = 3;


            var res = await controller.GetById(id);
            var ok = (OkObjectResult)res.Result;
            var shipment = (ShipmentsDetailedRead)ok.Value;


            res.Result.Should().BeOfType(typeof(OkObjectResult));
            shipment.ReceiverName.Should().Be(expectedShipment.ReceiverName);
            shipment.WeightOption.Should().Be(expectedShipment.WeightOption);
            shipment.DestinationCountry.Id.Should().Be(expectedShipment.DestinationCountryId);
            shipment.BoxColour.Should().Be(expectedShipment.BoxColour);
            shipment.ShipmentStatus.Count.Should().Be(numStatus);
        }


        [Test, Order(1)]
        public async Task GetByCustomerId_GetExistingUser_ReturnTheUsersOrders()
        {
            var controller = GetAdminShipmentsController();

            var id = 1;
            var numShipments = 3;
            

            var res = await controller.GetByCustomerId(id);
            var ok = (OkObjectResult)res.Result;
            var shipments = (List<ShipmentsRead>)ok.Value;


            res.Result.Should().BeOfType(typeof(OkObjectResult));
            shipments.Count.Should().Be(numShipments);

        }


        [Test, Order(1)]
        public async Task GetByCustomerId_GetNotExistingUser_NotFoundResult()
        {
            var controller = GetAdminShipmentsController();

            var id = -1;
            
            var res = await controller.GetByCustomerId(id);

            res.Result.Should().BeOfType(typeof(NotFoundResult));
        }


        [Test, Order(2)]
        public async Task UpdateShipmentStatus_UpdateNotExistingShipment_NotFoundResult()
        {
            var controller = GetAdminShipmentsController();

            var id = -1;
            var newStatus = new ShipmentStatusCreate {
                Status = ShipmentStatusType.COMPLETED
            };

            var res = await controller.UpdateShipmentStatus(id, newStatus);

            res.Result.Should().BeOfType(typeof(NotFoundResult));
        }

        [Test, Order(2)]
        public async Task UpdateShipmentStatus_UpdateNotExistingShipmentForUser_NotFoundResult()
        {
            var controller = GetUserShipmentsController();

            var id = -1;
            var newStatus = new ShipmentStatusCreate
            {
                Status = ShipmentStatusType.CANCELLED
            };

            var res = await controller.UpdateShipmentStatus(id, newStatus);

            res.Result.Should().BeOfType(typeof(NotFoundResult));
        }


        [Test, Order(2)]
        public async Task UpdateShipmentStatus_UpdateToNotCancelld_BadRequestResult()
        {
            var controller = GetUserShipmentsController();

            var id = 2;
            var newStatus = new ShipmentStatusCreate
            {
                Status = ShipmentStatusType.INTRANSIT
            };

            var res = await controller.UpdateShipmentStatus(id, newStatus);

            res.Result.Should().BeOfType(typeof(BadRequestResult));
        }

        [Test, Order(2)]
        public async Task UpdateShipmentStatus_UpdateWithAdmin_TheNewStatus()
        {
            var controller = GetAdminShipmentsController();

            var id = 1;
            var newStatus = new ShipmentStatusCreate
            {
                Status = ShipmentStatusType.COMPLETED
            };

            var res = await controller.UpdateShipmentStatus(id, newStatus);
            var ok = (OkObjectResult)res.Result;
            var status = (ShipmentStatusRead)ok.Value;

            res.Result.Should().BeOfType(typeof(OkObjectResult));
            status.Status.Should().Be(newStatus.Status.ToString());
           
        }

        [Test, Order(2)]
        public async Task UpdateShipmentStatus_UpdateWithUser_TheNewStatus()
        {
            var controller = GetUserShipmentsController();

            var id = 2;
            var newStatus = new ShipmentStatusCreate
            {
                Status = ShipmentStatusType.CANCELLED
            };

            var res = await controller.UpdateShipmentStatus(id, newStatus);
            var ok = (OkObjectResult)res.Result;
            var status = (ShipmentStatusRead)ok.Value;

            res.Result.Should().BeOfType(typeof(OkObjectResult));
            status.Status.Should().Be(newStatus.Status.ToString());
        }





        [Test]
        public async Task Delete_DeleteNotExistingShipment_NotFoundResult()
        {
            var controller = GetAdminShipmentsController();

            var id = -1;

            var res = await controller.Delete(id);

            res.Should().BeOfType(typeof(NotFoundResult));
        }


        [Test]
        public async Task Delete_DeleteExistingShipment_NotFoundResult()
        {
            var controller = GetAdminShipmentsController();

            var id = 6;

            var res = await controller.Delete(id);

            res.Should().BeOfType(typeof(NoContentResult));
            context.Shipments.Find(id).Should().BeNull();
        }
    }

}
